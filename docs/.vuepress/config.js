module.exports = {
    title: '金合前端工具库',
    port: 3000,
    base: '/JHwebUtils/',
    dest: 'public',
    description: 'Just playing around',
    themeConfig: {
        logo: '/assets/img/金合.png',
        displayAllHeaders: true,
        lastUpdated: '最后更新时间',
        nav: [
            { text: '指南', link: '/' },
            { text: '公司官网', link: 'http://www.ikingtech.com/index/index.html' },
            { text: 'GitHub', link: 'http://www.ikingtech.com' },
        ],
        sidebar: [
            {
                title: 'Date',   // 必要的
                // path: '/date/',      // 可选的, 标题的跳转链接，应为绝对路径且必须存在
                collapsable: false, // 可选的, 默认值是 true,
                sidebarDepth: 2,    // 可选的, 默认值是 1
                children: [
                    {
                        path: '/date/format',
                        title: '格式化'
                    },
                    {
                        path: '/date/month',
                        title: '月历'
                    }
                ]
            },
            {
                title: 'TypeChecking',
				collapsable: false, 
                children: [
                    {
                        path: '/type/checking',
                        title: '格式校验'
                    }
                ],
                // initialOpenGroupIndex: -1 // 可选的, 默认值是 0
            },
            {
                title: 'Array',
				collapsable: false, 
                children: [
                    {
                        path: '/array/unrepeat',
                        title: '去重'
                    },
                    {
                        path: '/array/sort',
                        title: '排序'
                    }
                ],
                // initialOpenGroupIndex: -1 // 可选的, 默认值是 0
            }
        ]
    }
}